package com.vdab.repository;

import com.vdab.domain.GameObject;

import java.util.ArrayList;

public class GameRepository {

    public static ArrayList<GameObject> gamesArrayList = new ArrayList<GameObject>();

    public GameRepository(ArrayList<GameObject> games) {
        this.gamesArrayList = games;
    }


    public GameRepository() {

    }

    public static ArrayList<GameObject> getGamesArrayList() {
        return gamesArrayList;
    }

    public static void addAGame(GameObject game){
        gamesArrayList.add(game);
    }
}
