package com.vdab.repository;

import com.vdab.domain.ConsoleObject;

import java.util.ArrayList;

public class ConsoleRepository {
    public static ArrayList<ConsoleObject> consoleRepository = new ArrayList<ConsoleObject>();

    public ConsoleRepository() {
    }

    public static ArrayList<ConsoleObject> getConsoleRepository() {
        return consoleRepository;
    }

}
