package com.vdab.domain;

public class GameObject {
    String name;

    public GameObject(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
